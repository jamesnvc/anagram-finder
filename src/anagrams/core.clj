(ns anagrams.core
  (:gen-class)
  (:require
   [clojure.core.reducers :as r]
   [clojure.string :as string]
   [clojure.java.io :as io]))

(defn gen-primes
  ([] (gen-primes (iterate inc (bigint 2))))
  ([ps]
   (cons (first ps)
         (lazy-seq
           (gen-primes
             (remove #(= 0 (mod % (first ps)))
                     (rest ps)))))))

(def primes (gen-primes))

(def letters
  (into #{} (map char) (range (int \a) (inc (int \z)))))

(def char->prime
  (zipmap letters primes))

(def product (partial reduce * 1))

(defn word->number
  [s]
  (->> s
      (string/lower-case)
      seq
      (filter letters)
      (map char->prime)
      product))

(defn anagram?
  [s1 s2]
  (= (word->number s1)
     (word->number s2)))

(defn load-dict
  []
  (with-open [f (io/reader (io/resource "words.txt"))]
    (loop [line (.readLine f)
           words []]
      (if (nil? line)
        words
        (recur (.readLine f)
               (conj words [(word->number line) line]))))))

(defonce dict (load-dict))

(defn anagram-search
  [w]
  (let [needle (word->number w)
        anagramable? (fn [n] (and (<= n needle) (= 0 (mod needle n))))
        possible-words (->> dict
                           (filter (fn [[n _]] (anagramable? n)))
                           #_(remove (fn [[_ w]] (and (not= w "a")
                                                   (not= w "I")
                                                   (<= (count w) 1))))
                           #_(remove (fn [[_ w]] (and (not= w "I")
                                                   (re-matches #"^[A-Z].*$" w))))
                           (map (fn [[n w]] [n [w]])))
        next-combos (fn [potentials]
                      (r/foldcat
                        (r/mapcat
                          (fn [[n words]]
                            (r/foldcat
                              (->> possible-words
                                  (r/map (fn [[m [w]]] [(* n m) (conj words w)]))
                                  (r/filter (fn [[n*m _]] (anagramable? n*m))))))
                          potentials)))]
    ;; new approach: loop over current list of candidates, store any
    ;; that are exactly needle, discard any that are greater than
    ;; needle *or* don't factor it, then form new combinations of the
    ;; single values with the remaining values, until the remaining
    ;; values is empty
    (loop [candidates possible-words
           anagrams #{}]
      (prn "candidates" (count candidates))
      (if (empty? candidates)
        anagrams
        (let [{anas :ana possible :can}
              (->> candidates
                  (group-by (fn [[n words]]
                              (cond
                                (= n needle) :ana
                                (anagramable? n) :can
                                :else nil))))
              next-candidates (next-combos possible)]
          (recur next-candidates (into anagrams (map second anas))))))))

(defn -main
  [& args]
  (println "Anagrams of " (first args))
  (doseq [words (anagram-search (first args))]
    (println words)))
