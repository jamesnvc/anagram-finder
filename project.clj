(defproject anagrams "0.0.1"
  :source-paths ["src"]

  :dependencies [[org.clojure/clojure "1.9.0"]]

  :main anagrams.core
  :profiles {:uberjar {:aot :all}})
